PFont font;

int WORLD_WIDTH = 800;
int WORLD_HEIGHT = 800;
PVector GOAL_POS = new PVector(WORLD_WIDTH/2, 10);
PVector SUBJECT_INIT_POS = new PVector(WORLD_WIDTH /2, WORLD_HEIGHT - 10);
int SUBJECT_MAX_MOVES = 1000;
float SUBJECT_FREEDOM_RATE = 0.10;
int GENERATION_SIZE = 1000;

int bestRecord = SUBJECT_MAX_MOVES;
Generation generation = new Generation(0);
ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();

void setup() {
  size(800, 800);
  frameRate(1000);
  font = createFont("Arial", 16, true);

  // init 1st gen
  for (int i = 0; i< GENERATION_SIZE; i++) {
    generation.subjects.add(new Subject());
  }

  // Obstacles
  obstacles.add(new Obstacle(200, 550, 600, 10));
  obstacles.add(new Obstacle(0, 250, 600, 10));
}

void draw() { 
  background(255);

  // Draw goal
  fill(255, 0, 0);
  ellipse(GOAL_POS.x, GOAL_POS.y, 10, 10);

  // Draw obstacles
  for (Obstacle obstacle : obstacles) {
    obstacle.render(#0000ff);
  }

  // Draw info
  textFont(font, 16);
  fill(0);        
  text("Gen #"+ generation.id + ", best: "+ bestRecord, 10, 50);

  // Process update
  if (generation.isAllDone()) {
    // Generation ended
    generation.calculateRating();
    ArrayList<Subject> bestSubjects = selectBestSubjects(generation);
    generation = makeNewGeneration(generation.id + 1, bestSubjects);
    generation.createVisualizeParents();
  } else {
    // Process and show
    generation.update();
    generation.renderAllSubjects();
  }
}
