class Subject {
  Brain brain = new Brain();
  PVector pos = SUBJECT_INIT_POS.copy();
  int moves = 0;

  // Set by others
  float rating = 0;
  boolean isActive = true;
  boolean isReachedGoal = false;

  private final PVector vel = new PVector(0, 0);

  void decideAndMove() {
    PVector nextMove = brain.makeDecision(moves);

    // Move
    vel.add(nextMove);
    vel.limit(5);
    pos.add(vel);

    moves++;
  }

  float distTo(PVector target) {
    return dist(pos.x, pos.y, target.x, target.y);
  }

  Subject giveBirth() {
    Subject child = new Subject();
    child.brain = brain.clone();
    return child;
  }
}
