class Generation {
  final int id;
  private ArrayList<Subject> subjects = new ArrayList<Subject>();
  private ArrayList<Subject> parents = new ArrayList<Subject>();
  private ArrayList<Subject> visualizeParents = new ArrayList<Subject>();

  Generation(int id) {
    this.id = id;
  }
  
  // Create visualize parent to display purpose
  void createVisualizeParents() {
    for (Subject parent : parents) {
      Subject subject = parent.giveBirth();
      subject.brain.freedomRate = 0;
      visualizeParents.add(subject);
    }
  }

  void update() {
    for (Subject subject : subjects) {
      updateSubject(subject);
    }
    
    for (Subject subject : visualizeParents) {
      updateSubject(subject);
    }
  }

  private void updateSubject(Subject subject) {
    // Check state
    if (subject.isActive) {

      // Reach max move allowed
      if (subject.moves >= bestRecord) {
        subject.isActive = false;
      }

      // Collide
      if (isCollide(subject.pos)) {
        subject.isActive = false;
      }

      // Check if reach goal
      if (subject.distTo(GOAL_POS) < 5) {
        subject.isReachedGoal = true;
        bestRecord = min(subject.moves, bestRecord);
        subject.isActive = false;
      }
    }

    // Make decision and move if active
    if (subject.isActive) {
      subject.decideAndMove();
    }
  }

  void renderAllSubjects() {
    fill(0);
    for (Subject subject : subjects) {
      ellipse(subject.pos.x, subject.pos.y, 4, 4);
    }
    
    fill(#00FF00);
    for (Subject subject : visualizeParents) {
      ellipse(subject.pos.x, subject.pos.y, 8, 8);
    }
  }

  boolean isAllDone() {
    for (Subject subject : subjects) {
      if (subject.isActive) {
        return false;
      }
    }
    return true;
  }

  public void calculateRating() {
    for (int i=0; i<subjects.size(); i++) {
      calculateSubjectRating(subjects.get(i));
    }
  }
}
