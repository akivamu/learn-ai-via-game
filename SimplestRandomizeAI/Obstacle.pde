class Obstacle {
  int x; 
  int y; 
  int w; 
  int h;
  public Obstacle(int x, int y, int w, int h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  boolean isCollide(PVector pos) {
    return (x < pos.x && pos.x < x + w && y < pos.y && pos.y < y + h);
  }

  void render(int colr) {
    fill(colr);
    rect(x, y, w, h);
  }
}
