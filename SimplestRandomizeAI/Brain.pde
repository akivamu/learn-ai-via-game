class Brain {
  float freedomRate = SUBJECT_FREEDOM_RATE;
  ArrayList<PVector> memory = new ArrayList<PVector>();

  Brain clone() {
    Brain brain = new Brain();
    brain.freedomRate = freedomRate;
    brain.memory = new ArrayList<PVector>(memory);
    return brain;
  }

  // Algo to make new decision
  PVector makeDecision(int stepNumber) {
    PVector nextMove = null;

    // Case freedom kick in or no experience
    if (random(1) < freedomRate || stepNumber > memory.size() - 1) {
      // Still stupid, just a random move
      nextMove = PVector.fromAngle(random(2*PI));
    } else {
      nextMove = memory.get(stepNumber);
    }

    if (stepNumber > memory.size() - 1) {
      // Add new experience
      memory.add(nextMove);
    } else {
      // Overwrite memory slot
      memory.set(stepNumber, nextMove);
    }

    return nextMove;
  }
}
