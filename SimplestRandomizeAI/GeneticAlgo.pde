float TOP_PERCENTAGE_TO_PICK = 0.01;


ArrayList<Subject> selectBestSubjects(Generation gen) {
  // All candidate include parents
  ArrayList<Subject> candidates = new ArrayList<Subject>(gen.subjects);
  candidates.addAll(gen.parents);

  // Extract ratings
  float[] ratings = new float[candidates.size()];
  for (int i=0; i<ratings.length; i++) {
    ratings[i] = candidates.get(i).rating;
  }

  // Sort, highest first
  ratings = reverse(sort(ratings));

  // Take best from top percentage
  ArrayList<Subject> bestSubjects = new ArrayList<Subject>();

  for (int i=0; i<ratings.length * TOP_PERCENTAGE_TO_PICK; i++) {
    for (Subject candidate : candidates) {
      if (candidate.rating == ratings[i]) {
        bestSubjects.add(candidate);
        break;
      }
    }
  }

  return bestSubjects;
}

Generation makeNewGeneration(int id, ArrayList<Subject> parents) {
  Generation newGen = new Generation(id);
  newGen.parents = parents;

  // Generate children from parent
  for (int i = 0; i< GENERATION_SIZE; i++) {
    int randomParentIndex = (int)random(0, parents.size());
    Subject subject = parents.get(randomParentIndex).giveBirth();
    subject.pos = SUBJECT_INIT_POS.copy();
    newGen.subjects.add(subject);
  }

  return newGen;
}


// Algo to rate a subject
float calculateSubjectRating(Subject subject) {
  subject.rating = 0;
  if (subject.isReachedGoal) { //if the dot reached the goal then the fitness is based on the amount of steps it took to get there
    subject.rating = 1.0/16.0 + 10000.0/(float)(subject.moves * subject.moves);
  } else {//if the dot didn't reach the goal then the fitness is based on how close it is to the goal
    float distanceToGoal = subject.distTo(GOAL_POS);
    subject.rating = 1.0/(distanceToGoal * distanceToGoal);
  }
  return subject.rating;
}

// Detect dead
boolean isCollide(PVector pos) {
  if (pos.x< 2|| pos.y<2 || pos.x>WORLD_WIDTH-2 || pos.y>WORLD_HEIGHT -2) { 
    return true;
  }

  for (Obstacle obstacle : obstacles) {
    if (obstacle.isCollide(pos)) {
      return true;
    }
  }
  return false;
}
